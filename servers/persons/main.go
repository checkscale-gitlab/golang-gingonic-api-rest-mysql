package main

import (
	routers "Go_Gingonic_Server/routers"

	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()
	routers.NewApplicationController(r)
	r.Run(":3010")

}
