package controllers

import (
	models "Go_Gingonic_Server/models"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type IController interface {
	Gettest(c *gin.Context)
	GettestByID(c *gin.Context)
}

type Base struct {
}

type ModelApplication struct {
	models.Base
}

/* Test functions */
func (self *Base) Ok(c *gin.Context) {
	payload := gin.H{
		"code":    http.StatusOK,
		"message": http.StatusText(http.StatusOK),
	}
	c.JSON(http.StatusOK, payload)
}

/* Test functions */

func (self *Base) Gettest(c *gin.Context) { /* Get all */

	app := new(ModelApplication)
	b, err := app.Get_all_test1("select * from test")
	if err != nil {
		log.Fatalln(err)
	}

	c.JSON(http.StatusOK, b)

}
func (self *Base) GettestByID(c *gin.Context) { /* Get by id */
	app := new(ModelApplication)
	b, err := app.Get_all_test1("select * from test WHERE id=" + c.Param("id"))
	if err != nil {
		log.Fatalln(err)
	}

	c.JSON(http.StatusOK, b)

}
