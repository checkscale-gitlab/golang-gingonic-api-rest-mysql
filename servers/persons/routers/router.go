package routers

import (
	controllers "Go_Gingonic_Server/controllers"

	"github.com/gin-gonic/gin"
)

type Application struct {
	controllers.Base
}

func NewApplicationController(r *gin.Engine) controllers.IController {
	app := new(Application)

	rg := r.Group("/api/app")
	rg.GET("/tests", app.Gettest)
	rg.GET("/test/:id", app.GettestByID)
	return app
}
