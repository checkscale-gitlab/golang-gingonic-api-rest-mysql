package db

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func Connect() *sql.DB {
	var DBHOST = "mysql"
	var DBPORT = "3306"
	var DBUSER = "root"
	var DBPASS = "test"
	var DBNAME = "test_go_gql"
	var DRIVER = "mysql"

	con, erro := sql.Open(DRIVER, fmt.Sprintf("%s:%s@tcp("+DBHOST+":"+DBPORT+")/%s", DBUSER, DBPASS, DBNAME))

	if erro != nil {
		panic(erro.Error())
	}
	return con
}
